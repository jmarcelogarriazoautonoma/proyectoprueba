import {PopoverContent,PopoverTrigger,Popover} from "./ui/popover"
import { Menu} from "lucide-react"; 
import Link from "next/link";
const ItemsMenuMobile=()=>{
    return(
        <Popover>
            <PopoverTrigger>
                <Menu/>
            </PopoverTrigger>
                <PopoverContent>
                    <Link href="/categorias/zapato-hombre" className="block">Zapato de Hombre </Link>
                    <Link href="/categorias/zapato-mujer" className="block">Zapato de Mujer</Link>
                    <Link href="/categorias/zapato-nino" className="block">Zapato de Niño</Link>
                </PopoverContent>
        </Popover>
    )
}
export default ItemsMenuMobile;