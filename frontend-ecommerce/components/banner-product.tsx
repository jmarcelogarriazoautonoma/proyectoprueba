import Link from "next/link"
import { buttonVariants } from "./ui/button"

const BannerProduct = () => {
    return (
        <>
            <div className="mt-4 text-center">
                <p>Sumérgete en una experiencia única</p>
                <h4 className="mt-2 text-3xl font-semibold ">Zapatos extravagantes</h4>
                <p className="my-2 text-lg">Despierta tus gustos con los mejores zapatos</p>
                <Link href="#" className={buttonVariants({ variant: 'outline' })}>Comprar</Link>

            </div>
            <div className=" h-[400px] lg:h-[600px] bg-[url('/assets/banner1.jpg')] bg-center bg-no-repeat bg-cover mt-5">
            </div>

        </>
    )
}
export default BannerProduct; 