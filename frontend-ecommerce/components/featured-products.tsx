"use client";

import { useGetFeaturedProducts } from "@/api/useGetFeaturedProducts";
import { ResponseType } from "@/types/response";
import { Expand, ShoppingCart } from "lucide-react";
import { Carousel, CarouselContent, CarouselItem, CarouselNext, CarouselPrevious } from "./ui/carousel";
import SkeletonSchema from "./skeletonSchema";
import { Card, CardContent } from "./ui/card";
import { ProductType } from "@/types/product";
import IconButton from "./icons-button";
import { useRouter } from "next/navigation";

const FeaturedProducts = () => {
    const router = useRouter();
    const { result, loading }: ResponseType = useGetFeaturedProducts();
    console.log(result);
    return (
        <>
            <div className="max-w-6xl py-4 mx-auto sm:py-16 sm:px-24">
                <h3 className="px-6 text-3xl sm:pb-8">Calzados destacados</h3>
                <Carousel>
                    <CarouselContent className="-ml-2 md:-ml-4">
                        {loading && (
                            <SkeletonSchema grid={3} />
                        )}
                        {Array.from({ length: 6 }).map((_, i) => {
                            return (
                                <CarouselItem key={i} className="md:basis-1/2 lg:basis-1/3 group">
                                    <div className="p-1">
                                        <Card className="py-4 border border-gray-200 shadow-none">
                                            <CardContent className="relative flex items-center justify-center px-6 py-2">
                                                <img src="https://via.placeholder.com/250x125" alt="Placeholder" />
                                                <div className="absolute w-full px-6 transition duration-200 opacity-0 group-hover:opacity-100">
                                                    <div className="flex justify-center gap-x-6">
                                                        <IconButton onClick={() => router.push(``)} icon={<Expand size={20} />} className="text-gray-600" />
                                                        <IconButton onClick={() => router.push(``)} icon={<ShoppingCart size={20} />} className="text-gray-600" />
                                                    </div>
                                                </div>
                                            </CardContent>
                                            <div className="flex justify-between gap-4 px-8 py-4">
                                                <h3 className="text-lg font-bold">Cuero Womman</h3>
                                                <div className="flex items-center justify-between gap-3">
                                                    <p className="px-2 py-1 text-white bg-red-600 rounded-full w-fit">-20%</p>
                                                    <p className="px-2 py-1 text-white bg-green-600 rounded-full w-fit">$150</p>
                                                </div>
                                            </div>
                                        </Card>
                                    </div>
                                </CarouselItem>
                            );
                        })}
                    </CarouselContent>
                    <CarouselPrevious/>
                    <CarouselNext className="hidden sm:flex"/>
                </Carousel>
            </div>
        </>
    );
};

export default FeaturedProducts;
