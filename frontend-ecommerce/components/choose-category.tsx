"use client";

import { useGetCategories } from "@/api/getProducts";
import { ChevronLeft, ChevronRight } from "lucide-react";
import { ResponseType } from "@/types/response";
import { useState } from "react";
import Link from "next/link";

const ChooseCategory = () => {
  const { result, loading }: ResponseType = useGetCategories();
  const [currentIndex, setCurrentIndex] = useState(0);
  const categories = Array.from({ length: 10 }).map((_, i) => ({
    id: i + 1,
    slug: `category-${i + 1}`,
    mainImage: {
      url: `https://via.placeholder.com/270x270?text=Calzado+${i + 1}`,
    },
    categoryName: `Calzado ${i + 1}`,
  }));
  const totalItems = categories.length;

  const handlePrev = () => {
    setCurrentIndex((prevIndex) => (prevIndex === 0 ? totalItems - 1 : prevIndex - 1));
  };

  const handleNext = () => {
    setCurrentIndex((prevIndex) => (prevIndex === totalItems - 1 ? 0 : prevIndex + 1));
  };

  return (
    <div className="w-full py-4 mx-auto sm:py-16 sm:px-24 relative">
      <h3 className="px-6 text-3xl sm:pb-4 font-bold text-center">Elige una categoría favorita</h3>
      <p className="text-center font-semibold">Combina con unos de estos y obtén el 20% de descuento</p>
      <div className="relative grid grid-colums-4 py-8 w-full overflow-hidden">
        <div
          className="flex transition-transform duration-500"
          style={{ transform: `translateX(-${currentIndex * 100}%)` }}
        >
          {categories.map((category) => (
            <Link
              key={category.id}
              href={`/category/${category.slug}`}
              className="relative flex-shrink-0 w-full sm:w-1/3 h-64 bg-no-repeat bg-cover"
              style={{ backgroundImage: `url(${category.mainImage.url})` }}
            >
              <p className="absolute bottom-0 w-full py-2 text-lg font-bold text-center text-white bg-black bg-opacity-50">
                {category.categoryName}
              </p>
            </Link>
          ))}
        </div>
        <button
          onClick={handlePrev}
          className="absolute top-1/2 left-4 transform -translate-y-1/2 bg-black bg-opacity-50 p-2 rounded-full z-10"
        >
          <ChevronLeft size={24} className="text-white" />
        </button>
        <button
          onClick={handleNext}
          className="absolute top-1/2 right-4 transform -translate-y-1/2 bg-black bg-opacity-50 p-2 rounded-full z-10"
        >
          <ChevronRight size={24} className="text-white" />
        </button>
      </div>
    </div>
  );
};

export default ChooseCategory;
