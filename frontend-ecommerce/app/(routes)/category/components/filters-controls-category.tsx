import FilterOrigin from "./filter-origin";
 type FilterControlCategoryProps={
    setFilterOrigin:(origin:string)=>void
 }
const FilterControlsCategory=(props:FilterControlCategoryProps)=>{
    const {setFilterOrigin}=props
    return(
        <div className="sm:w-[350px] sm:mt-5">
            <FilterOrigin setFilterOrigin={setFilterOrigin}/>
        </div>
        
    )
}
export default FilterControlsCategory;