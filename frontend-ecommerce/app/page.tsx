
import BannerDiscount from "@/components/banner-discount";
import BannerProduct from "@/components/banner-product";
import CarouselTextBanner from "@/components/carousel-text-banner";
import ChooseCategory from "@/components/choose-category";
import FeaturedProducts from "@/components/featured-products";
export default function Home() {
  return (
    <main className="w-full max-w-full p-0 m-0">
       <CarouselTextBanner/>
       <FeaturedProducts/>
       <BannerDiscount/>
       <ChooseCategory/>
       <BannerProduct/>
    </main>
  );
}
